import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import fsolve

# Set value
t_half = 4.5  # hours for ethanol
ke = np.log(2) / t_half  # Elimination rate constant
tmaxs = [0.5, 0.75, 1]  # tmax for long drinks, wine, and beer, respectively


# Function to find the root (ka) for given tmax
def equation(ka, tmax):
    return (np.log(ka / ke) / (ka - ke)) - tmax


# Estimate ka for each tmax
ka_estimates = []
for tmax in tmaxs:
    # Initial guess for ka
    initial_guess = 1.0

    # Estimate ka using fsolve
    ka_estimated = fsolve(equation, initial_guess, args=(tmax))[0]
    ka_estimates.append(ka_estimated)


drinks = ["Long Drink", "Wine", "Beer"]
for drink, ka in zip(drinks, ka_estimates):
    print(f"Estimated ka for {drink}: {ka:.2f}")

reaction_rates = [[ka, ke] for ka in ka_estimates]
S = np.array([[-1, 0], [1, -1]])
x0_init = 200
x1_init = 0
initial_state_vector = [x0_init, x1_init]


def propensities(state_vector, reaction_rates):
    R = np.zeros(2)
    R[0] = state_vector[0] * reaction_rates[0]
    R[1] = state_vector[1] * reaction_rates[1]
    return R


def ODE(stoi_matrix, prop_fun, state_vector, rates):
    return np.dot(stoi_matrix, prop_fun(state_vector, rates))


def ExplicitEuler(init_state_vector,
                  time_step,
                  time_final,
                  prop_fun,
                  ODE_fun,
                  stoi_matrix,
                  reaction_rates):
    NumSteps = int(time_final/time_step)
    times = np.zeros(NumSteps+1)
    states = np.zeros((np.size(times), len(init_state_vector)))
    states[0] = init_state_vector
    for step in range(NumSteps):
        times[step+1] = times[step] + time_step
        states[step+1] = states[step] + time_step * ODE_fun(stoi_matrix,
                                                            prop_fun,
                                                            states[step],
                                                            reaction_rates)
    return times, states


long_drink = 16  # mg
wine_glass = 24
beer = 25

# Define drinking sequence, respective rates and intervals
water = 0  # Last intake with 0mg of ethanol
drinking_sequence = [long_drink, wine_glass, beer, 2*beer, water]
rates_sequence = [reaction_rates[0],
                  reaction_rates[1],
                  reaction_rates[2],
                  reaction_rates[2]]
time_from_last_drink_to_party_end = 2
drinking_intervals = [1/2, 1/2, 1/3, time_from_last_drink_to_party_end]

state = [0, 0]
times_total = []
states_total = []
state = [drinking_sequence[0], 0]

for what, rate, time_interval in zip(drinking_sequence,
                                     rates_sequence,
                                     drinking_intervals):

    (times, states) = ExplicitEuler(init_state_vector=state,
                                    time_step=1/60,
                                    time_final=time_interval,
                                    prop_fun=propensities,
                                    ODE_fun=ODE,
                                    stoi_matrix=S,
                                    reaction_rates=rate)
    alcohol_conc_in_blood = [row[1] for row in states]
    alcohol_conc_in_digestive_tract = [row[0] for row in states]
    states_total.append(alcohol_conc_in_blood)
    times_total.append(times)
    state = [alcohol_conc_in_digestive_tract[-1] +
             what, alcohol_conc_in_blood[-1]]

states_total = [state for states_nested in states_total for state in states_nested]

# Calculate cumulative time
add = 0.00
times_cumulative = []
for row, time_interval in zip(times_total, drinking_intervals):
    for i in row:
        times_cumulative.append(i + add)
    add += time_interval

# Calculate promille
weight = 74
constant = (520 * weight)
promiles = [(state / constant) * 1000 for state in states_total]

# Determine safe times for driving
times_bicycle = []
times_car = []
threshold = 0.0009
for conc, time in zip(promiles, times_cumulative):
    if abs(conc - 0.5) <= threshold:
        times_car.append(time)
    elif abs(conc - 1.6) <= threshold:
        times_bicycle.append(time)

print("Times when the promiles are equal to the bicycle threshold: {}".format(times_bicycle))
print("Times when the promiles are equal to the car threshold: {}".format(times_car))


plt.figure(figsize=(10, 6))
plt.subplot(1, 3, 1)
plt.plot(times_cumulative, states_total, label="Alcohol concentration in the bloodstream")
plt.xlabel("time (h)")
plt.ylabel("alcohol concentration in bloodstream (ml/L)")
plt.legend()
plt.grid()

plt.subplot(1, 3, 2)
plt.plot(times_cumulative, promiles, label="promiles", color="green")
plt.axhline(y=0.5, color='blue', linestyle='-', label="car driving")
plt.axhline(y=1.5, color='red', linestyle='-', label="bicycle driving")
plt.xlabel("time (h)")
plt.ylabel("promiles (‰)")
plt.legend()
plt.grid()
plt.show()

